---
title: University | Sales Path
---

This page has been removed; please review the [Sales onboarding](https://about.gitlab.com/handbook/sales-onboarding) page instead.
