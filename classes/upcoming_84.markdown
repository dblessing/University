# Pivotal Cloud Foundry
# **<3**
# GitLab

---

# What is Cloud Foundry?

---

> Cloud Foundry is an open source platform for application lifecycle automation.

---

> An easier way to deploy and scale applications

---

![inline center](https://d1fto35gcfffzn.cloudfront.net/images/products/pivotal-cloud-foundry/cloud-native-diagram-azure.png)

---

# So what is _Pivotal_ Cloud Foundry?

---

![fit](/Users/job/Dropbox/Screenshots/Screenshot 2016-02-04 16.02.33.png)

---

![fit](/Users/job/Dropbox/Screenshots/Screenshot 2016-02-04 16.02.51.png)

---

# Advantages

- Easy installation (based on Omnibus)
- Super easy scaling
- Works with existing parts (MySQL cluster, Redis cluster)
- Highly Available from the Box
- Very Secure
- Easy upgrades
- Available for Plus Subscribers

---

# Disadvantages

- You can't edit code on the fly (easily)
- Harder to get into the server
- Only available to Pivotal Cloud Foundry customers

---

# Questions?

---
